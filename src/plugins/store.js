import { writable } from 'svelte/store'

export const log_state = writable({
    username: "",
    password: "",
})

export const bearer_token = writable("")