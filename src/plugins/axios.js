import axios from "axios";
import { log_state, bearer_token } from "./store";

let url = import.meta.env.VITE_BACKEND;

// add trailing slash if not present
if (url.substr(-1) !== "/") {
  url += "/";
}

const axiosConfig = {
  baseURL: url,
  timeout: 30000,
  withCredentials: true,
};

const back = axios.create(axiosConfig);

// Add a request interceptor to add the bearer token to the header
back.interceptors.request.use(
  (config) => {
    if (config.url === "/token") {
      return config;
    }
    bearer_token.subscribe((token) => {
      config.headers.Authorization = `Bearer ${token}`;
    });
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// Add a response interceptor to remove the token if it expired
back.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    // If unauthorized, try to get a new token
    if (error.response.status === 401) {
      bearer_token.subscribe((token) => {
        if (!token) {
          return;
        }
        log_state.subscribe((log) => {
          const params = new URLSearchParams();
          params.append("grant_type", "password");
          params.append("username", log.username);
          params.append("password", log.password);
          back
            .post("/token", params)
            .then((/** @type {{ data: { token: string; }; }} */ res) => {
              bearer_token.set(res.data.token);
              // Retry the request
              return back.request(error.config);
            })
            .catch((/** @type {any} */ err) => {
              console.log(err);
            });
        });
      });
    }
    return error;
  }
);

export default back;
